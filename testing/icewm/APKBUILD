# Contributor: Paul Bredbury <brebs@sent.com>
# Maintainer: gay <gay@disroot.org>
pkgname=icewm
pkgver=3.3.5
pkgrel=0
pkgdesc="Window manager designed for speed, usability and consistency"
url="https://github.com/ice-wm/icewm"
arch="all"
options="!check" # No test suite
license="LGPL-2.0-only"
subpackages="$pkgname-doc $pkgname-lang"
makedepends="
	alsa-lib-dev
	cmake
	fribidi-dev
	glib-dev
	imlib2-dev
	libao-dev
	libintl
	librsvg-dev
	libsm-dev
	libsndfile-dev
	libxcomposite-dev
	libxdamage-dev
	libxft-dev
	libxinerama-dev
	libxpm-dev
	libxrandr-dev
	markdown
	perl
	samurai
	"
source="https://github.com/ice-wm/icewm/releases/download/$pkgver/icewm-$pkgver.tar.lz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCFGDIR=/etc/icewm \
		-DENABLE_NLS=OFF \
		-DCONFIG_IMLIB2=ON \
		-DCONFIG_LIBRSVG=ON \
		-DENABLE_LTO=ON \
		-DDOCDIR=/usr/share/doc/icewm
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
954d4aeb766607cb00972e933dedc2d9d93c98896a3f4d2e25deb42cb08ba9682f199515c9caef6d0a83803bf82e2ad621d459c48dd76a6f305f66d728d5b4b5  icewm-3.3.5.tar.lz
"
