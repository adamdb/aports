# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=hypnotix
pkgver=3.2
pkgrel=0
pkgdesc="IPTV streaming application"
url="https://github.com/linuxmint/hypnotix"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	dconf python3 py3-cairo xapp py3-requests
	py3-setproctitle py3-imdbpy mpv mpv-libs
	"
makedepends="gettext-dev"
subpackages="$pkgname-lang"
options="!check" # No testsuite
source="$pkgname-$pkgver.tar.gz::https://github.com/linuxmint/hypnotix/archive/$pkgver.tar.gz
	0001-fix-locale-has-no-attribute-bindtextdomain.patch
	"

prepare() {
	default_prepare
	sed -i "s/__DEB_VERSION__/$pkgver/g" "$builddir"/usr/lib/hypnotix/hypnotix.py
}

build() {
	make
}

package() {
	install -D     -t "$pkgdir"/usr/bin usr/bin/hypnotix
	install -D     -t "$pkgdir"/usr/lib/hypnotix usr/lib/hypnotix/hypnotix.py
	install -D     -t "$pkgdir"/usr/lib/hypnotix usr/lib/hypnotix/common.py
	install -Dm644 -t "$pkgdir"/usr/lib/hypnotix usr/lib/hypnotix/mpv.py
	install -Dm644 -t "$pkgdir"/usr/share/applications usr/share/applications/hypnotix.desktop
	install -Dm644 -t "$pkgdir"/usr/share/glib-2.0/schemas usr/share/glib-2.0/schemas/org.x.hypnotix.gschema.xml
	install -Dm644 -t "$pkgdir"/usr/share/hypnotix usr/share/hypnotix/*.css
	install -Dm644 -t "$pkgdir"/usr/share/hypnotix usr/share/hypnotix/*.ui
	install -Dm644 -t "$pkgdir"/usr/share/hypnotix usr/share/hypnotix/*.png
	install -Dm644 -t "$pkgdir"/usr/share/hypnotix/pictures usr/share/hypnotix/pictures/*.svg
	install -Dm644 -t "$pkgdir"/usr/share/hypnotix/pictures/badges usr/share/hypnotix/pictures/badges/*
	install -Dm644 -t "$pkgdir"/usr/share/icons/hicolor/scalable/apps usr/share/icons/hicolor/scalable/apps/hypnotix.svg

	# locales
	cp -a usr/share/locale "$pkgdir"/usr/share/locale
}

sha512sums="
517a1f657eb936566e639fdaab538205d1e473be18a110fbe6f5e1dbe64813b6cabe39572e500e89a5ff6fad8b48d5fae46b0b25014d3bf570154b1c2709b703  hypnotix-3.2.tar.gz
b54f92f043b5d3bcba31266e21a263a0ef3428c3b3afbb4f9fcca11df1230a1e547abaf3bc047e4c975f8941d5a31c46c1ac1dbb9c3281dcb48553f941968478  0001-fix-locale-has-no-attribute-bindtextdomain.patch
"
