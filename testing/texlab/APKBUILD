# Maintainer: Dominika Liberda <ja@sdomi.pl>
# Contributor: Dominika Liberda <ja@sdomi.pl>
pkgname=texlab
pkgver=5.6.0
pkgrel=1
pkgdesc="Implementation of the Language Server Protocol for LaTeX"
url="https://github.com/latex-lsp/texlab"
# limited by rust/cargo
# armhf - fails to build
arch="x86_64 armv7 aarch64 x86 ppc64le"
license="GPL-3.0-or-later"
makedepends="cargo cargo-auditable"
source="https://github.com/latex-lsp/texlab/archive/refs/tags/v$pkgver/texlab-v$pkgver.tar.gz"

# tests OOM on 32-bit
# x86_64/ppc64le tests broken with some things in /tmp
case "$CARCH" in
	x86|x86_64|ppc64le|armv7) options="!check" ;;
esac

export CARGO_PROFILE_RELEASE_PANIC="unwind"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/texlab -t "$pkgdir"/usr/bin/
}

sha512sums="
82ce55e88b6d52671c507489a93bf395a5b88ffe9bbbcae8904ede2f25274bb79b41c02c213469e0ff741b276a0734190d150413142c843dcc9affbe5b95e738  texlab-v5.6.0.tar.gz
"
