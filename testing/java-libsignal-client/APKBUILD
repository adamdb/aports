# Contributor: Simon Frankenberger <simon-alpine@fraho.eu>
# Maintainer: Simon Frankenberger <simon-alpine@fraho.eu>
pkgname=java-libsignal-client
pkgver=0.25.0
pkgrel=0
pkgdesc="libsignal-client contains platform-agnostic APIs useful for Signal client apps"
url="https://github.com/signalapp/libsignal"
# * disabled on aarch64 due to:
#    error[E0554]: #![feature] may not be used on the stable release channel
#    --> /home/buildozer/.cargo/registry/src/github.com-1ecc6299db9ec823/polyval-0.5.3/src/lib.rs:80:5
# * rust and cargo not available on s390x and riscv64
# jdk17 only available on 64 bit archs
# ppc64le hangs in build
arch="x86_64"
license="AGPL-3.0-or-later"
depends="java-jre-headless"
makedepends="bash cargo clang-dev cmake openjdk11-jdk protoc rust zip"
source="$pkgname-$pkgver.tar.gz::https://github.com/signalapp/libsignal/archive/v$pkgver.tar.gz"
install="$pkgname.post-install $pkgname.post-upgrade"
builddir="$srcdir/libsignal-$pkgver"
# tests succeed, but gradle aborts with exit value 134. have to further investigate later
options="!check"


build() {
	cd "$builddir"/java
	./gradlew --no-daemon -PskipAndroid :client:jar
}

check() {
	cd "$builddir"/java
	./gradlew --no-daemon -PskipAndroid :client:test
}

package() {
	install -D -m644 "$builddir"/java/client/build/libs/libsignal-client-$pkgver.jar \
		-t "$pkgdir"/usr/share/java/libsignal-client
	install -D -m755 "$builddir"/target/release/libsignal_jni.so \
		-t "$pkgdir"/usr/lib
	zip -d "$pkgdir"/usr/share/java/libsignal-client/libsignal-client-$pkgver.jar \
		libsignal_jni.so
}

sha512sums="
1992eb77f25dc6a6ce526497be75d07905b8a49ea3d702705bedb35baf18296cffc70f6690604c64fbe8962643cad2fe2f5d78e0de6cf9d788a72ae6da8252b4  java-libsignal-client-0.25.0.tar.gz
"
