# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=rathole
pkgver=0.4.7
pkgrel=2
pkgdesc="High-performance reverse proxy for NAT traversal"
url="https://github.com/rapiz1/rathole"
license="Apache-2.0"
arch="all !s390x !riscv64" # blocked by rust/cargo
makedepends="cargo openssl-dev cargo-auditable"
source="https://github.com/rapiz1/rathole/archive/v$pkgver/rathole-$pkgver.tar.gz"
options="!check" # loop forever on failure

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rathole -t "$pkgdir"/usr/bin/
}

sha512sums="
3e061ec94df57cc8fa4b722ca9fae2fedfc66ea378f476a7b905fd604464808f456d333a190686f66bb946959c9f3669c57140e4a6e19c2aad41e5eafa629672  rathole-0.4.7.tar.gz
"
