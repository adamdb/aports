# Maintainer: psykose <alice@ayaya.dev>
pkgname=hwdata
pkgver=0.370
pkgrel=0
pkgdesc="Hardware identification and configuration data"
url="https://github.com/vcrhonek/hwdata"
arch="noarch"
license="GPL-2.0-or-later OR XFree86-1.1"
subpackages="$pkgname-dev $pkgname-usb $pkgname-pci $pkgname-pnp $pkgname-net"
source="$pkgname-$pkgver.tar.gz::https://github.com/vcrhonek/hwdata/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # just firmware data

replaces="hwids"
# be higher since hwids was a large date version
provides="hwids=20220101-r$pkgrel"

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--disable-blacklist
}

package() {
	depends="
		$pkgname-usb=$pkgver-r$pkgrel
		$pkgname-pci=$pkgver-r$pkgrel
		$pkgname-pnp=$pkgver-r$pkgrel
		$pkgname-net=$pkgver-r$pkgrel
		"
	make -j1 DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	depends="$pkgname=$pkgver-r$pkgrel"
}

usb() {
	pkgdesc="$pkgdesc (usb data)"
	provides="hwids-usb=20220101-r$pkgrel"
	replaces="hwids-usb"

	amove usr/share/hwdata/usb.ids
}

pci() {
	pkgdesc="$pkgdesc (pci data)"
	provides="hwids-pci=20220101-r$pkgrel"
	replaces="hwids-pci"

	amove usr/share/hwdata/pci.ids
}

net() {
	pkgdesc="$pkgdesc (net data)"
	provides="hwids-net=20220101-r$pkgrel"
	replaces="hwids-net"

	amove usr/share/hwdata/oui.txt
	amove usr/share/hwdata/iab.txt
}

pnp() {
	pkgdesc="$pkgdesc (pnp data)"

	amove usr/share/hwdata/pnp.ids
}

sha512sums="
a7a3e7685dd6beecf19cd177082b36b2f799f580f1b5b3838495948a686b81f1a981d9335faae9a52a3d5845eed950357e0a6ac5a178564ce54ddef00448bce4  hwdata-0.370.tar.gz
"
