# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-tools
_pkgname=Vulkan-Tools
pkgver=1.3.246.1
pkgrel=1
arch="all"
url="https://www.khronos.org/vulkan"
pkgdesc="Vulkan Utilities and Tools"
license="Apache-2.0"
makedepends="
	cmake
	glslang-dev
	libx11-dev
	libxrandr-dev
	python3
	samurai
	vulkan-headers
	vulkan-loader-dev
	wayland-dev
	wayland-protocols-dev
	"
source="https://github.com/KhronosGroup/Vulkan-Tools/archive/refs/tags/sdk-$pkgver/vulkan-tools-sdk-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-sdk-$pkgver"
options="!check" # No tests

build() {
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_DATADIR=/usr/share \
		-DCMAKE_SKIP_RPATH=True \
		-DBUILD_CUBE=ON \
		-DBUILD_VULKANINFO=ON \
		-DGLSLANG_INSTALL_DIR=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
db3038960c0dfcb473f0746f774883b7f0585370b5dd2426a65edc2b91697819a8746c794557b42f4864947cec9c4b39830cf96788eb7d2243f9f71cdbcaf2ca  vulkan-tools-sdk-1.3.246.1.tar.gz
"
