# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-headers
_pkgname=Vulkan-Headers
# Please be VERY careful upgrading this - vulkan-headers breaks API even
# on point releases. So please make sure everything using this still builds
# after upgrades
pkgver=1.3.246.1
pkgrel=0
arch="noarch"
url="https://www.vulkan.org/"
pkgdesc="Vulkan header files"
license="Apache-2.0"
makedepends="cmake samurai"
source="https://github.com/khronosgroup/vulkan-headers/archive/refs/tags/sdk-$pkgver/vulkan-headers-v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-sdk-$pkgver"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
31388837abebf2c3787008aa1b0b13aaa347934b9797f62b2455023eac9c65553c2294288821588f68b3623182215454f06adf11d1828d829ddef092dd7e4da4  vulkan-headers-v1.3.246.1.tar.gz
"
