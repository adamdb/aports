# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=marble
pkgver=23.04.1
pkgrel=0
pkgdesc="A Virtual Globe and World Atlas that you can use to learn more about Earth"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url='https://marble.kde.org'
license="LGPL-2.1-or-later AND GPL-3.0-or-later"
depends_dev="
	gpsd-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kparts-dev
	krunner-dev
	kwallet-dev
	phonon-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtserialport-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	qt5-qtwebengine-dev
	shared-mime-info
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/marble-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Requires itself to be installed

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DMOBILE=ON \
		-DBUILD_MARBLE_APPS=YES
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
24a7f6db5395742f864fb0cca76d59516d158d1f20e5de64feb7ab27620369f1bd44c8735985ac60a3f6cabdc894fc880b30a03af9d2f444383ce6242a24beb3  marble-23.04.1.tar.xz
"
