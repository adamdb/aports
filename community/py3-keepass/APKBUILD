# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=py3-keepass
pkgver=4.0.4
pkgrel=0
pkgdesc="Python3 library to interact with keepass databases"
url="https://github.com/libkeepass/pykeepass"
arch="noarch !s390x" # pykeepass test fail on s390x
license="GPL-3.0-only"
depends="
	py3-argon2-cffi
	py3-cffi
	py3-construct
	py3-dateutil
	py3-future
	py3-lxml
	py3-pycryptodomex
	python3
	"
makedepends="py3-setuptools"
subpackages="$pkgname-pyc"
source="
	https://github.com/libkeepass/pykeepass/archive/refs/tags/v$pkgver/py3-keepass-$pkgver.tar.gz
	0001-relax-dependencies.patch
	"
builddir="$srcdir/pykeepass-$pkgver"

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
49b806133ce92177225c49d82b4e3e3c994f6f4184049a2e1ed7d5afd52f46ce631f7da27289cb59bdc3fe2a89a03c9a3027fb8e8052e98772b16197e88b300c  py3-keepass-4.0.4.tar.gz
d75f21bde864b96abf9027a99bf3bcbf6549ea2cdd20909e4a7f1a5332c8924484d025d0e2df54769660a6ea345fa3bf4785c150df1e889ae89b4fc4ac45a864  0001-relax-dependencies.patch
"
