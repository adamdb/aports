# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaccounts-providers
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/internet/"
pkgdesc="Small system to administer web accounts for the sites and services across the KDE desktop"
license="GPL-2.0-or-later"
depends="
	signon-plugin-oauth2
	"
makedepends="
	extra-cmake-modules
	kaccounts-integration-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kpackage-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwebengine-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaccounts-providers-$pkgver.tar.xz"
options="!check" # No tests
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
151c6f65b306a47816b8ae79dbaa98d0fb7bd256a47428888fdab4776535caace77b021f688290047eaf179c32974c1690cf62270b45564bf0e21a3cecae42ed  kaccounts-providers-23.04.1.tar.xz
"
