# Maintainer: psykose <alice@ayaya.dev>
pkgname=py3-awscrt
pkgver=0.16.17
pkgrel=1
pkgdesc="Python bindings for the AWS Common Runtime"
url="https://github.com/awslabs/aws-crt-python"
# s390x: big endian is explicitly unsupported
# arm*, ppc64le: aws-crt-cpp
arch="all !armhf !armv7 !ppc64le !s390x"
license="Apache-2.0"
# use the cpp one to just pull the aws stack
makedepends="
	aws-crt-cpp-dev
	openssl-dev
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	samurai
	"
checkdepends="py3-websockets"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver-2.tar.gz::https://github.com/awslabs/aws-crt-python/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/aws-crt-python-$pkgver"
options="net" # tests need internet

case "$CARCH" in
arm*|aarch64|ppc64le)
	# too slow at running tests / fix later
	options="$options !check"
	;;
esac
prepare() {
	default_prepare

	# by default it's just 1.0.0.dev0
	echo "__version__ = '$pkgver'" >> awscrt/__init__.py

	# dynlink
	sed -i '/:lib/d' setup.py
}

build() {
	export AWS_CRT_BUILD_USE_SYSTEM_LIBCRYPTO=1
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m unittest discover test
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
0e1c2cc15d85c2aacaa5360a0f11dbc31c09d79b5c1fa7718de66f8f7125c8038a86289ac483bf9983eba5ae7682073856dac78794169bba1025124324f1defe  py3-awscrt-0.16.17-2.tar.gz
"
