# Maintainer: psykose <alice@ayaya.dev>
pkgname=apt
pkgver=2.7.0
pkgrel=0
pkgdesc="APT package management tool"
url="https://salsa.debian.org/apt-team/apt"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	bzip2-dev
	db-dev
	cmake
	dpkg-dev
	eudev-dev
	gettext-dev
	gnutls-dev
	libgcrypt-dev
	lz4-dev
	samurai
	triehash
	xxhash-dev
	xz-dev
	zlib-dev
	zstd-dev
	"
checkdepends="gtest-dev"
subpackages="
	$pkgname-dev
	$pkgname-libs
	"
source="https://salsa.debian.org/apt-team/apt/-/archive/$pkgver/apt-$pkgver.tar.bz2"
options="!check" # todo

build() {
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DWITH_DOC=OFF \
		-DUSE_NLS=ON \
		-DWITH_TESTS="$(want_check && echo ON || echo OFF)"
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# libraries only
	cd "$pkgdir"
	rm -r usr/bin usr/libexec usr/share var etc
}

sha512sums="
aec8603b535d0b89007b6a967150f1633900b3f0f3191c85bb84a78ef8069fcd5e838858ad22ba23b8e3c2e29e39d27438b0d23e2edbbdc91d1ca2fdbf3b7f76  apt-2.7.0.tar.bz2
"
