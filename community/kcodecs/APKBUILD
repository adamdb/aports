# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcodecs
pkgver=5.106.0
pkgrel=0
pkgdesc="Provide a collection of methods to manipulate strings using various encodings"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	gperf
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcodecs-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(rfc2047|kcharsets)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
319fc6c132d1a642b3b1f8edc53b908687a196c1b5a9ac0bffbdb2c6381d1e354c397d09527efa2d8e84ef2b2b8629bd78dc2f7b3ae73531489e46ba48b3588c  kcodecs-5.106.0.tar.xz
"
