# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesu
pkgver=5.106.0
pkgrel=0
pkgdesc="Integration with su for elevated privileges"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kpty-dev
	kservice-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdesu-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
# Since the goal of this library is to elevate privileges, suid being needed should be obvious
options="suid"

[ "$CARCH" = "ppc64le" ] && options="$options !check" # kdesutest is broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
da0a39ac4597c82dd7abb0aeb07014b2028a646c1f2ca4f380a018a12da5e1872ca709192a5035ecac81ccffb9c38dede8304f614037f06e143ca42f467fd583  kdesu-5.106.0.tar.xz
"
