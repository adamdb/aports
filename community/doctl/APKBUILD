# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=doctl
pkgver=1.96.0
pkgrel=0
pkgdesc="Official command line interface for the DigitalOcean API"
url="https://github.com/digitalocean/doctl"
license="Apache-2.0"
arch="all"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/digitalocean/doctl/archive/v$pkgver/doctl-$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	maj_min=${pkgver%*.*}
	major=${maj_min%.*}
	minor=${maj_min#*.}
	patch=${pkgver#*.*.*}

	go build \
		-mod=readonly \
		-ldflags "-extldflags \"$LDFLAGS\" \
			-X github.com/digitalocean/doctl.Major=$major \
			-X github.com/digitalocean/doctl.Minor=$minor \
			-X github.com/digitalocean/doctl.Patch=$patch \
			-X github.com/digitalocean/doctl.Label=alpine-$pkgrel" \
		./cmd/...
}

check() {
	go test -mod=readonly ./integration
}

package() {
	install -Dm755 doctl -t "$pkgdir"/usr/bin/

	# setup completions
	mkdir -p "$pkgdir"/usr/share/bash-completion/completions \
		"$pkgdir"/usr/share/zsh/site-functions \
		"$pkgdir"/usr/share/fish/completions

	"$pkgdir"/usr/bin/doctl completion bash > "$pkgdir"/usr/share/bash-completion/completions/doctl
	"$pkgdir"/usr/bin/doctl completion zsh > "$pkgdir"/usr/share/zsh/site-functions/_doctl
	"$pkgdir"/usr/bin/doctl completion fish > "$pkgdir"/usr/share/fish/completions/doctl.fish
}

sha512sums="
a9d2a6bcad8029aa72b58569a2cdb053b9e8bdf54f71e99f09ee0da7741c1774b7870b96423e69c10e8e517c17cd6671695ec368ffa8de1e70c784ea6012c874  doctl-1.96.0.tar.gz
"
