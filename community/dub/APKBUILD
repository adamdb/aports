# Contributor: Mathias LANG <pro.mathias.lang@gmail.com>
# Maintainer: Mathias LANG <pro.mathias.lang@gmail.com>
pkgname=dub
pkgver=1.32.1
pkgrel=0
pkgdesc="Package and build management system for D"
url="https://code.dlang.org/"
arch="x86_64 aarch64"
license="MIT"
depends="libcurl"
makedepends="chrpath ldc bash curl-dev"
subpackages="$pkgname-zsh-completion $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/dlang/dub/archive/v$pkgver.tar.gz"

# pass on edge but fail on 3.16(edge) ..
[ "$CARCH" = "aarch64" ] && options="$options !check"

build() {
	# Default abuild.conf has '-Os' which gdmd doesn't support
	# Since we later build the man pages using dub, make it persistent
	export DFLAGS=""

	# The build script needs a DMD-like interface to pass its arguments, so use ldmd2
	DMD=ldmd2 GITVER="v$pkgver" ldc2 -run "$builddir/build.d"

	# Build man pages
	"$builddir/bin/dub" --compiler=ldc2 --single scripts/man/gen_man.d

	# Remove redundant rpath
	chrpath -d "$builddir/bin/dub"
}

check() {
	bin/dub test
}

package() {
	install -s -D "$builddir/bin/dub" "$pkgdir/usr/bin/dub"
	install -Dm644 "$builddir/scripts/zsh-completion/_dub" "$pkgdir/usr/share/zsh/site-functions/_dub"
	mkdir -p "$pkgdir/usr/share/man/man1/"
	cp "$builddir"/scripts/man/*.1 "$pkgdir/usr/share/man/man1/"
}

sha512sums="
6ed022c3b3ee8fbf77da6ee82c6074fb4cca02db187598185cf4340e1fc161cc1f29ac20d73481a69a5b7ab85875cfd6be66fc6582749816a4a531a84cc945cc  dub-1.32.1.tar.gz
"
