# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kidletime
pkgver=5.106.0
pkgrel=0
pkgdesc="Monitoring user activity"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only"
depends_dev="qt5-qtx11extras-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	plasma-wayland-protocols
	qt5-qttools-dev
	qt5-qtwayland-dev
	samurai
	wayland-dev
	wayland-protocols
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kidletime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# solidmttest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "solidmttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1fefc848ebe36e00d9ee968e8a3bf57eb6c19a3edb862ed856eb8b41eff5cc524c465d48978befe154613f49b1e4bda384fce46da875b1b02f178b07a6eb3492  kidletime-5.106.0.tar.xz
"
