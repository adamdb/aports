# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=gallery-dl
pkgver=1.25.4
pkgrel=0
pkgdesc="CLI tool to download image galleries"
url="https://github.com/mikf/gallery-dl"
arch="noarch"
license="GPL-2.0-or-later"
depends="py3-requests python3"
makedepends="py3-setuptools"
checkdepends="py3-pytest yt-dlp"
subpackages="
	$pkgname-doc
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikf/gallery-dl/archive/v$pkgver.tar.gz"

build() {
	python3 setup.py build

	make man completion
}

check() {
	make test
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	# Install fish completion to the correct directory
	mv "$pkgdir"/usr/share/fish/vendor_completions.d "$pkgdir"/usr/share/fish/completions
}

sha512sums="
163033515925f13b8d19c98a45ef50e316be92cbaf439322a47d074a783519c389a685005b7f33a9d384791315d53cb2d59af030ab96ff8eaed7caa31f0e8e17  gallery-dl-1.25.4.tar.gz
"
