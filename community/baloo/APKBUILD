# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=baloo
pkgver=5.106.0
pkgrel=0
pkgdesc="A framework for searching and managing metadata"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND ( LGPL-2.1-only OR LGPL-3.0-only )"
depends_dev="
	kbookmarks-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kfilemetadata-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	kjobwidgets-dev
	kservice-dev
	lmdb-dev
	qt5-qtdeclarative-dev
	solid-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/baloo-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Tons of broken tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
4270bca2bb7a0a82383b195d87689df4a868dc6ba510d2aee2d26aeb853451c437ac9340bcb6954acba5a1c8c92fb36b8702dfabb4c5f5ab61bc17f7def61318  baloo-5.106.0.tar.xz
"
