# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kio-admin
pkgver=23.04.1
pkgrel=0
pkgdesc="Manage files as administrator using the admin:// KIO protocol"
url="https://invent.kde.org/system/kio-admin"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-3-Clause AND (GPL-2.0-only OR GPL-3.0-only)"
# zstd is purely used to unpack the source archive
makedepends="
	extra-cmake-modules
	ki18n-dev
	kio-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kio-admin-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7c7920daec7d686830eb8f97023d8765401967a1da6ae0941d39861c0961b5a352c4932731d908d6bc35e1d6468f0c26e44a68768e880d29b8172c3d4812337e  kio-admin-23.04.1.tar.xz
"
