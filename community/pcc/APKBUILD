# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=pcc
pkgver=20230419
pkgrel=0
pkgdesc="The portable C compiler."
url="http://pcc.ludd.ltu.se/"
arch="x86 x86_64"
license="BSD"
depends="pcc-libs-dev~=$pkgver"
makedepends="byacc flex"
subpackages="$pkgname-doc"
source="ftp://pcc.ludd.ltu.se/pub/pcc/pcc-$pkgver.tgz"

prepare() {
	default_prepare
	update_config_sub

	sed -i -e 's/AC_CHECK_PROG(strip,strip,yes,no)//' \
		configure.ac
	sed -i -e 's/AC_SUBST(strip)//' \
		configure.ac
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-stripping
	make
}

package() {
	make DESTDIR="$pkgdir" install

	# Don't conflict with gcc-doc.
	mv "$pkgdir"/usr/share/man/man1/cpp.1 \
		"$pkgdir"/usr/share/man/man1/$pkgname-cpp.1
}

sha512sums="
e4a5b8aa7a22210e8731852eeccbd16bdf3914011842ddc1295d1c72b3e2af05baac4583dc1a6c14d59818410c5595313d5924a188dfb279a51f22bfb4feaeb7  pcc-20230419.tgz
"
