# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=haruna
pkgver=0.11.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/multimedia/haruna"
pkgdesc="Open-source video player built with Qt/QML and libmpv"
license="GPL-2.0-or-later AND GPL-3.0-or-later AND BSD-3-Clause"
depends="
	kirigami2
	yt-dlp
	"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	mpv-dev
	ffmpeg-dev
	breeze-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kirigami2-dev
	kxmlgui-dev
	samurai
	"
source="https://download.kde.org/stable/haruna/haruna-$pkgver.tar.xz"
subpackages="$pkgname-lang $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
40150890892dd397642747326d17457727809c52825d09a5a5e1fb7c148b571defe66b748da81b8cd451093d5fd5806581f2b6ef9784170c9e31cfd152bc3ae7  haruna-0.11.0.tar.xz
"
