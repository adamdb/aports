# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kxmlgui
pkgver=5.106.0
pkgrel=0
pkgdesc="User configurable main windows"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	attica-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="
	mesa-dri-gallium
	xvfb-run
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kxmlgui-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kxmlgui_unittest, ktoolbar_unittest and ktooltiphelper_unittest are broken
	LC_ALL=C CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E '(kxmlgui|ktoolbar|ktooltiphelper)_unittest'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
91d15372a31acec317cb53821947076ed5c1d0cea00a4638f93d5359b56c340958759e153d5c78bb67482156d940b6a87cf0f6000e0d17fd2cba306e4bd6b706  kxmlgui-5.106.0.tar.xz
"
