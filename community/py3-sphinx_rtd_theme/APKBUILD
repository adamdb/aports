# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-sphinx_rtd_theme
pkgver=1.2.1
pkgrel=0
pkgdesc="Sphinx theme for readthedocs.org"
url="https://github.com/readthedocs/sphinx_rtd_theme"
arch="noarch"
license="MIT"
depends="
	py3-docutils
	py3-sphinx
	py3-sphinxcontrib-jquery
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
options="!check" # readthedocs_ext is missing
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/readthedocs/sphinx_rtd_theme/archive/$pkgver.tar.gz"
builddir="$srcdir/sphinx_rtd_theme-$pkgver"

replaces="py-sphinx_rtd_theme" # Backwards compatibility
provides="py-sphinx_rtd_theme=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
cf3448798e1f7098c41c425448d8865716b61dc5eceb565d4aa9e4ef8b49cc48da9a1ba34ecbeb0e2fabc4a1883472b5946e87b42ad19a1d5150dcd81d4240fa  py3-sphinx_rtd_theme-1.2.1.tar.gz
"
