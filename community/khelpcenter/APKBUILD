# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=khelpcenter
pkgver=23.04.1
pkgrel=0
pkgdesc="Application to show KDE Applications' documentation"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://userbase.kde.org/KHelpCenter"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	kinit-dev
	kservice-dev
	kwindowsystem-dev
	libxml2-dev
	qt5-qtbase-dev
	samurai
	xapian-core-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/khelpcenter-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
90f6bf87c6aab417fd1a153df8fffc2c4f53ae1dda1e4d917384fc3e78988a896cdb3e2803b100f108f4e5b087d0a0c9fee6fa0ff434844c36c21f3bd53b638c  khelpcenter-23.04.1.tar.xz
"
