# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
# filezilla needs to be rebuilt when libfilezilla version changes, ABI is not stable
pkgname=libfilezilla
pkgver=0.42.2
pkgrel=1
pkgdesc="C++ library for filezilla"
url="https://filezilla-project.org/"
arch="all"
license="GPL-2.0-or-later"
makedepends="gettext gnutls-dev linux-headers nettle-dev"
checkdepends="cppunit-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.filezilla-project.org/libfilezilla/libfilezilla-$pkgver.tar.xz"

build() {
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
a453854f3a7143a63a2fc79458096e9b8f2f6a07f480cb15a0c0873a4cc147c0373d6f1647e1dd3dece015fbdedc3ce3085d4c7d26c801c0948fd1c593fd6d8c  libfilezilla-0.42.2.tar.xz
"
