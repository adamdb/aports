# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=openmp
pkgver=16.0.4
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="LLVM OpenMP Runtime Library"
url="https://openmp.llvm.org/"
# s390x: LIBOMP_ARCH = UnknownArchitecture
# armhf: doesn't match arm baseline
arch="all !armhf !s390x"
license="Apache-2.0"
depends_dev="
	$pkgname=$pkgver-r$pkgrel
	"
makedepends="
	clang
	cmake
	elfutils-dev
	libffi-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	perl
	samurai
	"
checkdepends="llvm$_llvmver-test-utils"
subpackages="$pkgname-dev"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/openmp-${pkgver//_/}.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/cmake-${pkgver//_/}.src.tar.xz
	"
builddir="$srcdir/$pkgname-${pkgver//_/}.src"
options="!check" # todo

case "$CARCH" in
aarch64|ppc64le|x86_64)
	depends_dev="
		$depends_dev 
		$pkgname-bitcode=$pkgver-r$pkgrel
		"
	subpackages="
		$subpackages
		$pkgname-bitcode
		libomptarget
		libomptarget-rtl-cuda
		libomptarget-rtl-cuda-nextgen:cuda_nextgen
		libomptarget-rtl-amdgpu
		libomptarget-rtl-amdgpu-nextgen:amdgpu_nextgen
		libomptarget-rtl-nextgen
		libomptarget-rtl
		"
	;;
riscv64)
	depends_dev="
		$depends_dev 
		$pkgname-bitcode=$pkgver-r$pkgrel
		"
	subpackages="
		$subpackages
		$pkgname-bitcode
		libomptarget
	"
esac

prepare() {
	default_prepare
	mv "$srcdir"/cmake-${pkgver//_/}.src "$srcdir"/cmake
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CC=clang \
	CXX=clang++ \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLIBOMP_INSTALL_ALIASES=OFF \
		-DCLANG_TOOL=/usr/bin/clang \
		-DLINK_TOOL=/usr/bin/llvm-link \
		-DOPT_TOOL=/usr/bin/opt \
		-DPACKAGER_TOOL=/usr/bin/clang-offload-packager \
		-DOPENMP_LLVM_TOOLS_DIR=/usr/lib/llvm$_llvmver/bin \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	cmake --build build --target check-openmp
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	rm -f "$pkgdir"/usr/lib/libarcher_static.a
}

libomptarget() {
	amove usr/lib/libomptarget.so.*
}

rtl() {
	amove usr/lib/libomptarget.rtl.*.so.*
}

nextgen() {
	amove usr/lib/libomptarget.rtl.*.nextgen.so.*
}

amdgpu() {
	amove usr/lib/libomptarget.rtl.amdgpu.so.*
}

amdgpu_nextgen() {
	amove usr/lib/libomptarget.rtl.amdgpu.nextgen.so.*
}

cuda_nextgen() {
	amove usr/lib/libomptarget.rtl.cuda.nextgen.so.*
}

cuda() {
	amove usr/lib/libomptarget.rtl.cuda.so.*
}

bitcode() {
	amove usr/lib/libomptarget*.bc
}

sha512sums="
ebe58859b5ffe56960f0a0743e7b4144bbdd245ea915d8357ce1052af436768f90861472a83e097793d0963fa4fb412810d03538b9c73396eb9d4b4980ad5b3d  openmp-16.0.4.src.tar.xz
942f10a5d1e3e48768d62a2595f8670872069ab2065871c786a435ae23108fb263e8c3db906dca0e68aeb8aad00f62f7604cd2f41da9e00f574b6021f846bb9d  cmake-16.0.4.src.tar.xz
"
