# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmousetool
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kmousetool"
pkgdesc="A program that clicks the mouse for you"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	knotifications-dev
	kxmlgui-dev
	phonon-dev
	libxt-dev
	libxtst-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmousetool-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
91d6b21f0ee12b4317200c679464fdcb5be341f011bfb8d525f28e2d3e33ffa6570882b6de1dda856afb1091a362dc21140b96e380d7b3142e2eddefd6c4c613  kmousetool-23.04.1.tar.xz
"
